//
//  CardTests.m
//  Dominion
//
//  Created by Jonas Illum on 03/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CardVault.h"
#import "Card.h"

@interface CardTests : XCTestCase

@property (nonatomic) CardVault *cardVault;
@property (nonatomic) WoodcutterCard *woodCutterCard;

@end


@implementation CardTests


- (void)setUp
{
    [super setUp];
    _cardVault = [CardVault sharedInstance];
    _woodCutterCard = [[WoodcutterCard alloc]init];

}


- (void)test_card_basicCreation
{
    CardType *actionAndAttackCardType = [[CardType alloc]initWithCardSubType:ActionSubType andSubType:AttackSubType];
    Card *testCard = [[Card alloc]initCardWithName:@"Test"
                                               text:@"Testing the Test Card"
                                           cardType:actionAndAttackCardType
                                              price:@5];
    XCTAssertTrue(testCard);
    XCTAssertEqual([testCard cardName], @"Test");
    XCTAssertEqual([testCard cardText], @"Testing the Test Card");
    XCTAssertTrue([[testCard cardType]isAction]);
    XCTAssertTrue([[testCard cardType]isAttack]);
    XCTAssertFalse([[testCard cardType]isShelter]);
    XCTAssertEqual([testCard cardPrice], @5);
}


- (void)test_copy_concreteCard
{
    WoodcutterCard *copyOfWoodcutterCard = [_woodCutterCard copy];
    XCTAssertNotNil(copyOfWoodcutterCard);
}


- (void)test_copy_CardFromVault
{
    Card *originalCard = [_cardVault produceCardFromName:@"Cellar"];
    XCTAssertNotNil(originalCard);
    Card *copyOfCard = [originalCard copy];
    XCTAssertNotNil(copyOfCard);
}


@end
