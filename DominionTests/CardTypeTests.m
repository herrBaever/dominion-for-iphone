//
//  CardTypeTests.m
//  Dominion
//
//  Created by Jonas Illum on 08/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CardType.h"

@interface CardTypeTests : XCTestCase

@end

@implementation CardTypeTests


- (void)test_cardType
{
    CardType *victoryType = [[CardType alloc]initWithCardSubType:VictorySubType];
    XCTAssertTrue(victoryType);
    XCTAssertTrue([victoryType isVictory]);
    XCTAssertFalse([victoryType isAttack]);
    XCTAssertEqual([victoryType subTypeCount], 1);
    
    CardType *actionAndReactionType = [[CardType alloc]initWithCardSubType:ActionSubType
                                                                andSubType:ReactionSubType];
    XCTAssertTrue(actionAndReactionType);
    XCTAssertTrue([actionAndReactionType isAction]);
    XCTAssertTrue([actionAndReactionType isReaction]);
    XCTAssertFalse([actionAndReactionType isVictory]);
    XCTAssertEqual([actionAndReactionType subTypeCount], 2);
    
    CardType *treasurePrizeAndKnight = [[CardType alloc]initWithCardSubTypes:TreasureSubType
                                                                  andSubType:PrizeSubType
                                                                  andSubType:KnightSubType];
    XCTAssertTrue([treasurePrizeAndKnight isTreasure]);
    XCTAssertTrue([treasurePrizeAndKnight isPrize]);
    XCTAssertTrue([treasurePrizeAndKnight isKnight]);
    XCTAssertFalse([treasurePrizeAndKnight isCurse]);
    XCTAssertEqual([treasurePrizeAndKnight subTypeCount], 3);
}


- (void)test_hasSubType
{
    CardType *actionAttackAndDuration = [[CardType alloc]initWithCardSubTypes:ActionSubType
                                                                  andSubType:AttackSubType
                                                                  andSubType:DurationSubType];
    
    XCTAssertTrue([actionAttackAndDuration hasSubType:ActionSubType]);
    XCTAssertTrue([actionAttackAndDuration hasSubType:AttackSubType]);
    XCTAssertTrue([actionAttackAndDuration hasSubType:DurationSubType]);
    
    XCTAssertFalse([actionAttackAndDuration hasSubType:ShelterSubType]);
    XCTAssertFalse([actionAttackAndDuration hasSubType:CurseSubType]);
    XCTAssertFalse([actionAttackAndDuration hasSubType:VictorySubType]);
    XCTAssertFalse([actionAttackAndDuration hasSubType:TreasureSubType]);
    XCTAssertFalse([actionAttackAndDuration hasSubType:ReactionSubType]);
    XCTAssertFalse([actionAttackAndDuration hasSubType:KnightSubType]);
    XCTAssertFalse([actionAttackAndDuration hasSubType:PrizeSubType]);
    XCTAssertFalse([actionAttackAndDuration hasSubType:LooterSubType]);
}


@end
