//
//  GameTests.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GamePreparation.h"
#import "Game.h"

@interface GameTests : XCTestCase

@property (nonatomic)GamePreparation *preparationOfNewGame;

@end


@implementation GameTests


- (void)setUp
{
    _preparationOfNewGame = [[GamePreparation alloc]init];
    [_preparationOfNewGame addHumanPlayerWithName:@"Signe"];
    [_preparationOfNewGame addHumanPlayerWithName:@"Kasper"];
    
    [_preparationOfNewGame addKingdomCardWithName:@"Witch"];
    [_preparationOfNewGame addKingdomCardWithName:@"Cellar"];
    [_preparationOfNewGame addKingdomCardWithName:@"Chapel"];
    [_preparationOfNewGame addKingdomCardWithName:@"Laboratory"];
    [_preparationOfNewGame addKingdomCardWithName:@"Market"];
    [_preparationOfNewGame addKingdomCardWithName:@"Remodel"];
    [_preparationOfNewGame addKingdomCardWithName:@"Smithy"];
    [_preparationOfNewGame addKingdomCardWithName:@"Village"];
    [_preparationOfNewGame addKingdomCardWithName:@"Woodcutter"];
    
    XCTAssertTrue([_preparationOfNewGame isGameReady]);
}


- (void)test_initialiseGame
{
    Game *newGame = [[Game alloc]initWithPreparation:_preparationOfNewGame];
    XCTAssertNotNil(newGame);
    XCTAssertEqual([newGame roundNumber], @1);
    XCTAssertNotNil([newGame cardSupply]);
    
    XCTAssertEqual([[newGame cardSupply]count], 9 + 7); // The 9 added card(stacks) from setUp, plus the 7 base card(stacks).
}



@end
