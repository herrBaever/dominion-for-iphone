//
//  CardDeckTests.m
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CardDeck.h"
#import "GoldCard.h"
#import "DuchyCard.h"
#import "MarketCard.h"
#import "CurseCard.h"
#import "RemodelCard.h"
#import "ChapelCard.h"
#import "SmithyCard.h"
#import "LaboratoryCard.h"

@interface CardDeckTests : XCTestCase

@property (nonatomic) GoldCard *goldCard;
@property (nonatomic) DuchyCard *duchyCard;
@property (nonatomic) MarketCard *marketCard;
@property (nonatomic) CurseCard *curseCard;
@property (nonatomic) RemodelCard *remodelCard;
@property (nonatomic) ChapelCard *chapelCard;
@property (nonatomic) SmithyCard *smithyCard;
@property (nonatomic) LaboratoryCard *laboratoryCard;

@end

@implementation CardDeckTests


- (void)setUp
{
    _goldCard = [[GoldCard alloc]init];
    _duchyCard = [[DuchyCard alloc]init];
    _marketCard = [[MarketCard alloc]init];
    _curseCard = [[CurseCard alloc]init];
    _remodelCard = [[RemodelCard alloc]init];
    _chapelCard = [[ChapelCard alloc]init];
    _smithyCard = [[SmithyCard alloc]init];
    _laboratoryCard = [[LaboratoryCard alloc]init];
}


- (void)test_initialisation
{
    CardDeck *mixedCardStack = [[CardDeck alloc]init];
    XCTAssertNotNil(mixedCardStack);
}


- (void)test_addSingleCards_mixed
{
    CardDeck *deck = [[CardDeck alloc]init];
    int lowerTestLimit = 0;
    int upperTestLimit = 80;
    
    for (int currentTestNumber = lowerTestLimit; currentTestNumber <= upperTestLimit; currentTestNumber += 8) // As in 8 cards added every iteration.
    {
        XCTAssertEqual([deck numberOfCards], currentTestNumber);
        [deck addCard:_marketCard];
        [deck addCard:_goldCard];
        [deck addCard:_duchyCard];
        [deck addCard:_curseCard];
        [deck addCard:_remodelCard];
        [deck addCard:_chapelCard];
        [deck addCard:_smithyCard];
        [deck addCard:_laboratoryCard];
    }
}


- (void)test_addNumberOfCards_mixed
{
    CardDeck *deck = [[CardDeck alloc]init];
    XCTAssertEqual([deck numberOfCards], 0);
    
    [deck addNumber:11 ofSameCard:_goldCard];
    XCTAssertEqual([deck numberOfCards], 0 + 11);
    
    [deck addNumber:5 ofSameCard:_curseCard];
    XCTAssertEqual([deck numberOfCards], 0 + 11 + 5);
    
    [deck addNumber:30 ofSameCard:_marketCard];
    XCTAssertEqual([deck numberOfCards], 0 + 11 + 5 + 30);
    
    [deck addNumber:4 ofSameCard:_goldCard];
    XCTAssertEqual([deck numberOfCards], 0 + 11 + 5 + 30 + 4);
    
    [deck addNumber:23 ofSameCard:_duchyCard];
    XCTAssertEqual([deck numberOfCards], 0 + 11 + 5 + 30 + 4 + 23);
}


// With probability theoretically there's a very small risk deck will end up in same order as pre-shuffling.
- (void)test_shuffleDeck
{
    CardDeck *deck = [[CardDeck alloc]init];
                                    // Position from "bottom" and "up"
    [deck addCard:_marketCard];     // 1 - First card in, last card out
    [deck addCard:_goldCard];       // 2
    [deck addCard:_duchyCard];      // 3
    [deck addCard:_curseCard];      // 4
    [deck addCard:_remodelCard];    // 5
    [deck addCard:_chapelCard];     // 6
    [deck addCard:_smithyCard];     // 7
    [deck addCard:_laboratoryCard]; // 8 - Last card in, first card out
    
    [deck shuffle]; // All this code to test if this little line is behaving.
    
    // draw method returns cards in LIFO
    // Lets decide that something is wrong with shuffling and therefore test should fail if half comes out in the same order.
    int sameOrderCount = 0;
    if ([[deck drawCard]isEqual:_laboratoryCard]) sameOrderCount++; // LIFO...
    if ([[deck drawCard]isEqual:_smithyCard]) sameOrderCount++;
    if ([[deck drawCard]isEqual:_chapelCard]) sameOrderCount++;
    if ([[deck drawCard]isEqual:_remodelCard]) sameOrderCount++;
    if ([[deck drawCard]isEqual:_curseCard]) sameOrderCount++;
    if ([[deck drawCard]isEqual:_duchyCard]) sameOrderCount++;
    if ([[deck drawCard]isEqual:_goldCard]) sameOrderCount++;
    if ([[deck drawCard]isEqual:_marketCard]) sameOrderCount++;
    
    // Future Jonas, I hope the logic have remained clear.
    
    // Now asserting than no more than half of the cards have same position as pre-shuffling.
    XCTAssertTrue(sameOrderCount < 4);
}

@end
