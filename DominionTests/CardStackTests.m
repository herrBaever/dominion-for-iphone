//
//  CardStackTests.m
//  Dominion
//
//  Created by Jonas Illum on 03/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CardStack.h"
#import "GoldCard.h"
#import "DuchyCard.h"
#import "MarketCard.h"
#import "CurseCard.h"

@interface CardStackTests : XCTestCase

@property (nonatomic) GoldCard *goldCard;
@property (nonatomic) DuchyCard *duchyCard;
@property (nonatomic) MarketCard *marketCard;
@property (nonatomic) CurseCard *curseCard;
@property (nonatomic) CardStack *marketTestStack;

@end



@implementation CardStackTests


- (void)setUp
{
    _goldCard = [[GoldCard alloc]init];
    _duchyCard = [[DuchyCard alloc]init];
    _marketCard = [[MarketCard alloc]init];
    _curseCard = [[CurseCard alloc]init];
    
    _marketTestStack = [[CardStack alloc]initWithCard:_marketCard andStackSize:10];
}


- (void)test_initialisation
{
    CardStack *goldStack = [[CardStack alloc]initWithCard:_goldCard andStackSize:4];
    XCTAssertNotNil(goldStack);
    XCTAssertEqual([goldStack numberOfCards], 4);
    
    CardStack *duchyStack = [[CardStack alloc]initWithCard:_duchyCard andStackSize:10];
    XCTAssertNotNil(duchyStack);
    
    CardStack *marketStack = [[CardStack alloc]initWithCard:_marketCard andStackSize:1];
    XCTAssertNotNil(marketStack);
    
    CardStack *curseStack_zero = [[CardStack alloc]initWithCard:_curseCard andStackSize:0];
    XCTAssertNil(curseStack_zero);
    
    CardStack *goldStack_negative = [[CardStack alloc]initWithCard:_curseCard andStackSize:-3];
    XCTAssertNil(goldStack_negative);
    
}


- (void)test_numberOfCards
{
    int lowerTestLimit = 0;
    int upperTestLimit = 80;
    
    for (int currentTestNumber = lowerTestLimit; currentTestNumber <= upperTestLimit; currentTestNumber++)
    {
        CardStack *testStack = [[CardStack alloc]initWithCard:_marketCard andStackSize:currentTestNumber];
        XCTAssertEqual([testStack numberOfCards], currentTestNumber);
    }
}


- (void)test_drawCard
{
    CardStack *duchyStack = [[CardStack alloc]initWithCard:_duchyCard andStackSize:2];
    Card *newDraw = [duchyStack drawCard];
    XCTAssertNotNil(newDraw);
    DuchyCard *duchyDraw = (DuchyCard *)[duchyStack drawCard];
    XCTAssertNotNil(duchyDraw);
    Card *nilCard = [duchyStack drawCard];
    XCTAssertNil(nilCard, @"Card stack should be empty by now and no cards should be drawn");
}


- (void)test_addSingleCard
{
    CardStack *marketStack = [[CardStack alloc]initWithCard:_marketCard andStackSize:1];
    XCTAssertEqual([marketStack numberOfCards], 1);
    [marketStack addCard:_marketCard];
    XCTAssertEqual([marketStack numberOfCards], 2);
}


- (void)test_addSingleCard_MultipleTimes
{
    CardStack *marketStack = [[CardStack alloc]initWithCard:_marketCard andStackSize:1];
    
    int lowerTestLimit = 1;
    int upperTestLimit = 80;
    
    for (int currentTestNumber = lowerTestLimit; currentTestNumber <= upperTestLimit; currentTestNumber++)
    {
        XCTAssertEqual([marketStack numberOfCards], currentTestNumber);
        [marketStack addCard:_marketCard];
    }
}


- (void)test_addNumberOfSameCard
{
    CardStack *goldStack = [[CardStack alloc]initWithCard:_goldCard andStackSize:20];
    XCTAssertEqual([goldStack numberOfCards], 20);
    
    [goldStack addNumber:55 ofSameCard:_goldCard];
    XCTAssertEqual([goldStack numberOfCards], 20 + 55);
    
    [goldStack addNumber:28 ofSameCard:_curseCard];
    XCTAssertNotEqual([goldStack numberOfCards], 20 + 55 + 28, @"No MarketCard should be added to non mixed stack of GoldCards");
}


@end
