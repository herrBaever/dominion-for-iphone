//
//  CardVaultTests.m
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CardVault.h"

@interface CardVaultTests : XCTestCase

@property (nonatomic) CardVault *cardVault;

@end


@implementation CardVaultTests

- (void)setUp
{
    _cardVault = [CardVault sharedInstance];
}


- (void)test_produceCardFromName
{
    Card *witchCard = [_cardVault produceCardFromName:@"Witch"];
    XCTAssertNotNil(witchCard);
    Card *shouldBeNil = [_cardVault produceCardFromName:@"This Is Not a Card Name"];
    XCTAssertNil(shouldBeNil);
}


- (void)test_singleton
{
    CardVault *sharedInstance = [CardVault sharedInstance];
    CardVault *sameInstance = [CardVault sharedInstance];
    XCTAssertEqual(sharedInstance, sameInstance);
}


- (void)test_numberOfCards
{
    XCTAssertEqual([[_cardVault allCards]count], [_cardVault numberOfCards]);
}


- (void)test_noCardsOfSameKind
{
    NSSet *uniqueCards = [[NSSet alloc]initWithArray:[_cardVault allCards]];
    XCTAssertEqual([_cardVault numberOfCards], [uniqueCards count]); // Number of cards in a set made from the vault, should be the same as number of cards in vault.
}


- (void)test_produceRandomUniqueKingdomCardsWithCount_range
{
    NSArray *random8KingdomCards = [_cardVault produceRandomUniqueKingdomCardsWithCount:8];
    XCTAssertNotNil(random8KingdomCards);
    XCTAssertEqual(8, [random8KingdomCards count]);
    
    NSArray *random1KingdomCards = [_cardVault produceRandomUniqueKingdomCardsWithCount:1];
    XCTAssertNotNil(random8KingdomCards);
    XCTAssertEqual(1, [random1KingdomCards count]);
    
    NSArray *requestTooHigh = [_cardVault produceRandomUniqueKingdomCardsWithCount:10 + 1];
    XCTAssertNil(requestTooHigh);
    
    NSArray *requestTooLow_0 = [_cardVault produceRandomUniqueKingdomCardsWithCount:0];
    XCTAssertNil(requestTooLow_0);
    
    NSArray *requestTooLow_Negative = [_cardVault produceRandomUniqueKingdomCardsWithCount:-1];
    XCTAssertNil(requestTooLow_Negative);
    
    
}


@end
