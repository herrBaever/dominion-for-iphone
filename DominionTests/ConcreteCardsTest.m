//
//  ConcreteCardsTest.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CardVault.h"

@interface ConcreteCardsTest : XCTestCase

@property (nonatomic)CardVault *cardVault;
@end

@implementation ConcreteCardsTest


- (void)setUp
{
    [super setUp];
    _cardVault = [CardVault sharedInstance];
}


- (void)test_witchCard
{
    Card *witchCard = [_cardVault produceCardFromName:@"Witch"];
    XCTAssertNotNil(witchCard);
    XCTAssert([[witchCard cardName]isEqualToString:@"Witch"]);
    XCTAssertTrue([[witchCard cardType]isAction]);
    XCTAssertTrue([[witchCard cardType]isAttack]);
    XCTAssertEqual([[witchCard cardType]subTypeCount], 2);
    XCTAssertEqual([witchCard cardPrice], @5);
}


- (void)test_CellarCard
{
    NSString *concreteCardName = @"Cellar";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @2);
}


- (void)test_ChapelCard
{
    NSString *concreteCardName = @"Chapel";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @2);
}


- (void)test_LaboratoryCard
{
    NSString *concreteCardName = @"Laboratory";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @5);
}


- (void)test_MarketCard
{
    NSString *concreteCardName = @"Market";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @5);
}


- (void)test_RemodelCard
{
    NSString *concreteCardName = @"Remodel";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @4);
}


- (void)test_SmithyCard
{
    NSString *concreteCardName = @"Smithy";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @4);
}


- (void)test_VillageCard
{
    NSString *concreteCardName = @"Village";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @3);
}


- (void)test_WoodcutterCard
{
    NSString *concreteCardName = @"Woodcutter";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @3);
}


- (void)test_WorkshopCard
{
    NSString *concreteCardName = @"Workshop";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isAction]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @3);
}


- (void)test_CopperCard
{
    NSString *concreteCardName = @"Copper";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isTreasure]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @0);
}


- (void)test_SilverCard
{
    NSString *concreteCardName = @"Silver";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isTreasure]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @3);
}


- (void)test_GoldCard
{
    NSString *concreteCardName = @"Gold";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isTreasure]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @6);
}


- (void)test_EstateCard
{
    NSString *concreteCardName = @"Estate";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isVictory]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @2);
}


- (void)test_DuchyCard
{
    NSString *concreteCardName = @"Duchy";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isVictory]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @5);
}


- (void)test_ProvinceCard
{
    NSString *concreteCardName = @"Province";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isVictory]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @8);
}


- (void)test_CurseCard
{
    NSString *concreteCardName = @"Curse";
    Card *concreteCard = [_cardVault produceCardFromName:concreteCardName];
    XCTAssertNotNil(concreteCard);
    XCTAssert([[concreteCard cardName]isEqualToString:concreteCardName]);
    XCTAssertTrue([[concreteCard cardType]isCurse]);
    XCTAssertEqual([[concreteCard cardType]subTypeCount], 1);
    XCTAssertEqual([concreteCard cardPrice], @0);
}


@end
