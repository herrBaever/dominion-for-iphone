//
//  DominionTests.m
//  DominionTests
//
//  Created by Jonas Illum on 29/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Player.h"


@interface DominionTests : XCTestCase

@end

@implementation DominionTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_Player
{
    Player *newPlayer = [[Player alloc]initWithName:@"TestName"];
    XCTAssertEqual([newPlayer playerName], @"TestName");
}


@end
