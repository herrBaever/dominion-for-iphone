//
//  CardDiscardPileTest.m
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CardDiscardPile.h"

#import "CellarCard.h"
#import "ChapelCard.h"
#import "LaboratoryCard.h"
#import "MarketCard.h"
#import "EstateCard.h"
#import "DuchyCard.h"
#import "ProvinceCard.h"
#import "CopperCard.h"
#import "SilverCard.h"
#import "GoldCard.h"


@interface CardDiscardPileTest : XCTestCase


@property (nonatomic) CellarCard     *cellar1Card;
@property (nonatomic) CellarCard     *cellar2Card;
@property (nonatomic) CellarCard     *cellar3Card;

@property (nonatomic) ChapelCard     *chapelCard;
@property (nonatomic) LaboratoryCard *laboratoryCard;

@property (nonatomic) MarketCard     *market1Card;
@property (nonatomic) MarketCard     *market2Card;

@property (nonatomic) EstateCard     *estateCard;

@property (nonatomic) DuchyCard      *duchy1Card;
@property (nonatomic) DuchyCard      *duchy2Card;

@property (nonatomic) ProvinceCard   *provinceCard;
@property (nonatomic) CopperCard     *copperCard;

@property (nonatomic) SilverCard     *silver1Card;
@property (nonatomic) SilverCard     *silver2Card;
@property (nonatomic) SilverCard     *silver3Card;

@property (nonatomic) GoldCard       *goldCard;


@property (nonatomic) NSArray *arrayOfMixedCards;


@end

@implementation CardDiscardPileTest


- (void)setUp
{
    _cellar1Card =   [[CellarCard alloc]init];
    _cellar2Card =   [[CellarCard alloc]init];
    _cellar3Card =   [[CellarCard alloc]init];
    
    _chapelCard =   [[ChapelCard alloc]init];
    _laboratoryCard = [[LaboratoryCard alloc]init];
    
    _market1Card =   [[MarketCard alloc]init];
    _market2Card =   [[MarketCard alloc]init];
    
    _estateCard =   [[EstateCard alloc]init];
    
    _duchy1Card =    [[DuchyCard alloc]init];
    _duchy2Card =    [[DuchyCard alloc]init];
    
    _provinceCard = [[ProvinceCard alloc]init];
    _copperCard =   [[CopperCard alloc]init];
    
    _silver1Card =   [[SilverCard alloc]init];
    _silver2Card =   [[SilverCard alloc]init];
    _silver3Card =   [[SilverCard alloc]init];
    
    _goldCard =     [[GoldCard alloc]init];
    
    _arrayOfMixedCards = @[_cellar1Card, _cellar2Card, _cellar3Card,
                           _chapelCard,
                           _laboratoryCard,
                           _market1Card, _market2Card,
                           _estateCard,
                           _duchy1Card, _duchy2Card,
                           _silver1Card, _silver2Card, _silver3Card
                          ];
}


- (void)test_initialisation
{
    CardDiscardPile *discardPile = [[CardDiscardPile alloc]init];
    XCTAssertNotNil(discardPile);
}


- (void)test_reshuffleIntoDeck
{
    CardDeck *playersDeck = [[CardDeck alloc]init];
    CardDiscardPile *playersDiscardPile = [[CardDiscardPile alloc]init];

    int cardCountForPile = [_arrayOfMixedCards count];
    [playersDiscardPile addMixedCards:_arrayOfMixedCards];

    XCTAssertEqual([playersDiscardPile numberOfCards], cardCountForPile);
    XCTAssertEqual([playersDeck numberOfCards], 0);
    
    [playersDiscardPile reshuffleIntoDeck:playersDeck];
    
    XCTAssertEqual([playersDiscardPile numberOfCards], 0);
    XCTAssertEqual([playersDeck numberOfCards], cardCountForPile);
}

@end
