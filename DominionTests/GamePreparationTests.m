//
//  GamePreparationTests.m
//  Dominion
//
//  Created by Jonas Illum on 03/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GamePreparation.h"
#import "Card.h"
#import "CardStack.h"

@interface GamePreparationTests : XCTestCase

@property (nonatomic) GamePreparation *preparationOfNewGame;

@end



@implementation GamePreparationTests


- (void)setUp
{
    _preparationOfNewGame = [[GamePreparation alloc]init];
}


- (void)test_addHumanPlayers
{
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 0);
    XCTAssertTrue([_preparationOfNewGame addHumanPlayerWithName:@"Olaf"]);
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 1);
    XCTAssertTrue([_preparationOfNewGame addHumanPlayerWithName:@"Christine"]);
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 2);
    XCTAssertTrue([_preparationOfNewGame addHumanPlayerWithName:@"Susanne"]);
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 3);
    XCTAssertTrue([_preparationOfNewGame addHumanPlayerWithName:@"Niels"]);
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 4);
    
    XCTAssertFalse([_preparationOfNewGame addHumanPlayerWithName:@"Lise"], @"Maximum players should be exeeded by adding a 5th player");
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 4);
}


- (void)test_illegal_addPlayerWithExistingName
{
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 0);
    XCTAssertTrue([_preparationOfNewGame addHumanPlayerWithName:@"Unique"]);
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 1);
    XCTAssertFalse([_preparationOfNewGame addHumanPlayerWithName:@"Unique"]);
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 1);
}


- (void)test_removePlayer
{
    [_preparationOfNewGame addHumanPlayerWithName:@"Bjarne"];
    [_preparationOfNewGame addHumanPlayerWithName:@"Thomas"];
    [_preparationOfNewGame addHumanPlayerWithName:@"Solvej"];
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 3);
    [_preparationOfNewGame removePlayerWithName:@"Thomas"];
    XCTAssertEqual([[_preparationOfNewGame playersAdded]count], 2);
}


- (void)test_addKingdomCards
{
    XCTAssertTrue([[_preparationOfNewGame  kingdomCardsChosen]count] == 0);
    [_preparationOfNewGame  addKingdomCardWithName:@"Witch"];
    Card *witchCard = [[_preparationOfNewGame  kingdomCardsChosen]firstObject];
    XCTAssertTrue([[witchCard cardName]isEqualToString: @"Witch"]);
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 1);
    [_preparationOfNewGame  addKingdomCardWithName:@"Market"];
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 2);
    [_preparationOfNewGame  addKingdomCardWithName:@"Village"];
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 3);
    [_preparationOfNewGame  addKingdomCardWithName:@"Cellar"];
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 4);
}


- (void)test_illegalCardAdding_RandomNamed
{
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 0);
    [_preparationOfNewGame  addKingdomCardWithName:@"This is not a card name, so kingdomCardsChosen count shouldn't rise"];
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 0);
}


- (void)test_illegalCardAdding_TreasureCards
{
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 0);
[_preparationOfNewGame  addKingdomCardWithName:@"Copper"];
    [_preparationOfNewGame  addKingdomCardWithName:@"Silver"];
    [_preparationOfNewGame  addKingdomCardWithName:@"Gold"];
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 0, @"Treasure cards shouldn't be added to kingdom cards");
}


- (void)test_illegalCardAdding_VictoryCards
{
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 0);
[_preparationOfNewGame  addKingdomCardWithName:@"Estate"];
    [_preparationOfNewGame  addKingdomCardWithName:@"Duchy"];
    [_preparationOfNewGame  addKingdomCardWithName:@"Province"];
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 0, @"Victory cards shouldn't be added to kingdom cards");
}


- (void)test_removeCard
{
    [_preparationOfNewGame  addKingdomCardWithName:@"Witch"];
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 1);
    [_preparationOfNewGame  removeKingdomCardWithName:@"Witch"];
    XCTAssertEqual([[_preparationOfNewGame  kingdomCardsChosen]count], 0);
}


- (void)test_constructCardSupply
{
    NSArray *cardSupply = [_preparationOfNewGame constructCardSupply];
    XCTAssertNil(cardSupply, @"0 players added, no supply should be constructed");
    [_preparationOfNewGame addHumanPlayerWithName:@"Simon"];
    cardSupply = [_preparationOfNewGame constructCardSupply];
    XCTAssertNil(cardSupply, @"1 player added, no supply should be constructed");
    [_preparationOfNewGame addHumanPlayerWithName:@"Henning"];
    
    cardSupply = [_preparationOfNewGame constructCardSupply];
    XCTAssertNil(cardSupply, @"Not enough kingdom cards added, no supply should be constructed");
    
    [_preparationOfNewGame addKingdomCardWithName:@"Market"];       // card 1
    [_preparationOfNewGame addKingdomCardWithName:@"Laboratory"];   // card 2
    [_preparationOfNewGame addKingdomCardWithName:@"Cellar"];       // card 3
    [_preparationOfNewGame addKingdomCardWithName:@"Witch"];        // card 4
    [_preparationOfNewGame addKingdomCardWithName:@"Remodel"];      // card 5
    [_preparationOfNewGame addKingdomCardWithName:@"Smithy"];       // card 6
    [_preparationOfNewGame addKingdomCardWithName:@"Village"];      // card 7
    [_preparationOfNewGame addKingdomCardWithName:@"Woodcutter"];   // card 8
    [_preparationOfNewGame addKingdomCardWithName:@"Workshop"];     // card 9
    
    cardSupply = [_preparationOfNewGame constructCardSupply];
    XCTAssertNotNil(cardSupply, @"Enough kingdom cards added, card supply should be constructed");
    XCTAssertEqual([cardSupply count], 9 + 7); // 9 kingdom cards + 7 base cards.
    
    for (CardStack *cardStack in cardSupply)
    {
        Card *firstCard = [cardStack drawCard];
        NSLog(@"%@ ", [firstCard cardName]);
    }
    
}





@end
