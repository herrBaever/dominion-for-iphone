Dominion is a physical deck-building card game. This project aims to convert the game to an app.

When a game begins, every player receives the same small fixed deck of cards. Through the game the players improves and expands their deck by buying new more powerful cards and upgrading or removing existing weaker ones.