//
//  GameButton.h
//  Dominion
//
//  Created by Jonas Illum on 13/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameButton : SKSpriteNode

- (instancetype)initWithImageNamed:(NSString *)name
                 notificationEvent:(NSString *)notification
                         labelName:(NSString *)labelName;

- (instancetype)initWithImageNamed:(NSString *)name
                 notificationEvent:(NSString *)notification;

- (void)enable;
- (void)disable;

@end
