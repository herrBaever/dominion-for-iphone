//
//  Game.h
//  Dominion
//
//  Created by Jonas Illum on 30/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GamePreparation.h"


@interface Game : NSObject

@property (nonatomic, readonly) NSNumber *roundNumber;
@property (nonatomic, readonly) NSMutableArray *playersInTurnOrder;
@property (nonatomic, readonly) NSArray *cardSupply;

- (instancetype)initWithPreparation:(GamePreparation *)gamePreparation;

@end
