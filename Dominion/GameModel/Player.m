//
//  Player.m
//  Dominion
//
//  Created by Jonas Illum on 01/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "Player.h"
#import "PlayerHand.h"
#import "CardDeck.h"
#import "CardDiscardPile.h"
#import "NotificationList.h"
#import "PlayerPlayField.h"

@interface Player ()

@property (nonatomic, readwrite) PlayerHand *handOfCards;
@property (nonatomic) CardDiscardPile *discardPile;
@property (nonatomic) CardDeck *deck;
@property (nonatomic, readwrite) PlayerPlayField *playField;

@end


@implementation Player


#pragma mark Initilisation

- (instancetype)initWithName:(NSString *)playerName
{
    self = [super init];
    if (self)
    {
        _playerName  = playerName;
        _discardPile = [[CardDiscardPile alloc]init];
        _handOfCards = [[PlayerHand alloc]init];
        _playField   = [[PlayerPlayField alloc]init];
    }
    return self;
}


#pragma mark Card Methods

- (void)giveCard:(Card *)card
{
    [_discardPile addCard:card];
}


- (void)drawCardFromDeckToHandWithCount:(int)count
{
    while (count > 0)
    {
        [self drawCardFromDeckToHand];
        count--;
    }
}


- (void)drawCardFromDeckToHand
{
    if ([_deck isEmpty])
    {
        [_discardPile reshuffleIntoDeck:_deck]; // TODO: Prio_low - Is this the most logical place to call for a reshuffle?
    }
    [_handOfCards addCard:[_deck drawCard]];
}


#pragma mark Phase Methods

- (void)beginTurn
{
    [self beginActionPhase];
    // TODO: Prio_high - Notify of turn start.
}


// TODO: Prio_low - Use a state machine?
- (void)beginActionPhase
{
    
    
}


- (void)beginBuyPhase
{
    
}


- (void)beginCleanupPhase
{
    [_discardPile addMixedCards:[_handOfCards transferAllCards]]; // During clean up all cards from hand are discarded.
    [_discardPile addMixedCards:[_playField cleanUp]];
    // TODO: Prio_high - Notify of turn end.
}


@end
