//
//  CardStack.h
//  Dominion
//
//  Created by Jonas Illum on 30/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"


@interface CardStack : NSObject

- (instancetype)initWithCard:(Card *)card andStackSize:(int)size;

- (int)numberOfCards;
- (bool)isEmpty;
- (Card *)drawCard; // TODO Prio_high - remove when specialised stacks have their functionality.
- (void)addCard:(Card *)card;
- (void)addNumber:(int)number ofSameCard:(Card *)card;
- (void)addMixedCards:(NSArray *)cards;

@end
