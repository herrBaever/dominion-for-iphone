//
//  CardStack_Extension.h
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardStack.h"


@interface CardStack ()

@property (nonatomic) NSMutableArray *cardsInTheStack;
@property (nonatomic) bool mixedCards;

@end
