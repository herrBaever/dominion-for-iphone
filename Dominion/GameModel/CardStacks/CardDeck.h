//
//  CardDeck.h
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardStack.h"


@interface CardDeck : CardStack

- (void)shuffle;
//- (NSArray *)drawHand; // TODO: Prio_low - implement?

@end
