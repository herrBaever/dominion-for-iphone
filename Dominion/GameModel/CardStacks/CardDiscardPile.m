//
//  CardDiscardPile.m
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardDiscardPile.h"
#import "CardStack_Extension.h"

@implementation CardDiscardPile


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setMixedCards:YES];
        [self setCardsInTheStack:[[NSMutableArray alloc]init]];
    }
    return self;
}


- (void)reshuffleIntoDeck:(CardDeck *)deck;
{
    [deck addMixedCards:[self cardsInTheStack]];
    [deck shuffle];
    [[self cardsInTheStack]removeAllObjects];
}

- (Card *)drawCard
{
    return nil;
}

@end
