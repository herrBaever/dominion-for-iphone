//
//  CardStack+Supply.m
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardStack+Supply.h"
#import "CardStack_Extension.h"

@implementation CardStack (Supply)


- (NSNumber *)coinCost
{
    return [[[self cardsInTheStack]firstObject]cardPrice];
}


- (void)buyCardAsPlayer:(Player *)player
{
    
}


@end
