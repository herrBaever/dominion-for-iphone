//
//  CardTrashPile.m
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardTrashPile.h"
#import "CardStack_Extension.h"

@implementation CardTrashPile


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setMixedCards:YES];
        [self setCardsInTheStack:[[NSMutableArray alloc]init]];
    }
    return self;
}


- (instancetype)initWithCard:(Card *)card andStackSize:(int)size
{
    return nil;
}


- (Card *)drawCard
{
    return nil;
}


@end
