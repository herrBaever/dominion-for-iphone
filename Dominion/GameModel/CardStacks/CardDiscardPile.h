//
//  CardDiscardPile.h
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardStack.h"
#import "CardDeck.h"


@interface CardDiscardPile : CardStack

- (void)reshuffleIntoDeck:(CardDeck *)deck;
- (Card *)drawCard __attribute__((unavailable("Can't draw cards from discard pile.")));

@end
