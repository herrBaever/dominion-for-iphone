//
//  CardTrashPile.h
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardStack.h"


@interface CardTrashPile : CardStack

- (instancetype)initWithCard:(Card *)card andStackSize:(int)size __attribute__((unavailable("Trash starts empty")));

- (Card *)drawCard __attribute__((unavailable("Can't draw cards from trash")));

@end
