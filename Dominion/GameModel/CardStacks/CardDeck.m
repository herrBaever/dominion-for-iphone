//
//  CardDeck.m
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardDeck.h"
#import "CardStack.h"
#import "CardStack_Extension.h"


@implementation CardDeck

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setMixedCards:YES];
        [self setCardsInTheStack:[[NSMutableArray alloc]init]];
    }
    return self;
}


- (void)shuffle
{
    int totalShuffles = [self numberOfCards];
    int shufflesLeftToDo = totalShuffles;
    int progressedToIndex = 0;
    while (shufflesLeftToDo > 0)
    {
        [self.cardsInTheStack exchangeObjectAtIndex:progressedToIndex
                                  withObjectAtIndex:arc4random_uniform(shufflesLeftToDo)]; // A card object can and must be able to end up on its original position.
        progressedToIndex++;
        shufflesLeftToDo--;
    }
}


@end
