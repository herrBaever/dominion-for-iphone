//
//  CardStack+Supply.h
//  Dominion
//
//  Created by Jonas Illum on 04/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardStack.h"
#import "Player.h"


@interface CardStack (Supply)

- (NSNumber *)coinCost;
- (void)buyCardAsPlayer:(Player *)player;

@end
