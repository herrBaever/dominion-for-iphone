//
//  CardStack.m
//  Dominion
//
//  Created by Jonas Illum on 30/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardStack.h"
#import "CardStack_Extension.h"
#import "Game.h"

// TODO: Prio_medium - Think about deck / discard pile vs supply
// TODO: Prio_medium - make abstract and implement new concrete supply stack?


@implementation CardStack


// TODO: Prio_low - Another approach could be to only initialise the top card and keep a simple count of how many cards that are left in stack "below" the top card. Would reduce the amount of Card objects by a considerable amount. On the other hand the data of a card object is so far quite low and in any case copies are shallow.
- (instancetype)initWithCard:(Card *)card andStackSize:(int)size
{
    if (size <= 0)
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        _mixedCards = NO;
        _cardsInTheStack = [[NSMutableArray alloc]init];
        for (int i = 0; i < size; i++)
        {
            [_cardsInTheStack addObject:[card copy]];
        }
        
    }
    return self;
}


- (int)numberOfCards
{
    return [_cardsInTheStack count];
}


- (bool)isEmpty
{
    return [_cardsInTheStack count] < 1;
}


// returns cards from stack in LIFO order
- (Card *)drawCard
{
    Card *drawnCard = [_cardsInTheStack lastObject];
    if (drawnCard) {
        [_cardsInTheStack removeLastObject];
    }
    return drawnCard;
}


- (void)addNumber:(int)number ofSameCard:(Card *)card
{
    if ([self isLegalInStack_Card:card])
    {
        for (int i = 0; i < number; i++)
        {
            [_cardsInTheStack addObject:card];
        }
    }
}


- (void)addMixedCards:(NSArray *)cards
{
    for (Card *card in cards)
    {
        if ([self isLegalInStack_Card:card])
        {
            [_cardsInTheStack addObject:card];
        }
    }
}


- (BOOL)isLegalInStack_Card:(Card *)card // How the bloody ... can you follow obj-c naming convention here? Switch to Yoda or German grammar?
{
    return [self mixedCards] || [card isMemberOfClass:[[_cardsInTheStack firstObject]class]];
}


- (void)addCard:(Card *)card
{
    [self addNumber:1 ofSameCard:card];
}


@end
