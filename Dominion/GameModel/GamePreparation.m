//
//  GamePreparation.m
//  Dominion
//
//  Created by Jonas Illum on 01/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "GamePreparation.h"
#import "Player.h"
#import "CardVault.h"
#import "CardStack.h"
#import "KingdomCardProtocol.h"


@interface GamePreparation ()
// TODO: Prio_medium - Convert player and card array to set for uniqueness checking, but then I need to read up on equality checking for sets in obj-c.
@property (nonatomic) NSMutableArray *playersOfTheGame;
@property (nonatomic) NSMutableArray *kingdomCardsChosen;
@property (nonatomic) CardVault *cardVault;
@property (nonatomic, readwrite) NSDictionary *cardStackSizes;

@end


@implementation GamePreparation


#pragma mark Initilisation


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _kingdomCardsChosen = [[NSMutableArray alloc]init];
        _playersOfTheGame = [[NSMutableArray alloc]init];
        _cardVault = [CardVault sharedInstance];
        _cardStackSizes = [self setupCardStackSizes];
        _minimumPlayers = 2;
        _maximumPlayers = 4;
        _minimumKingdomCards = 4;
        _maximumKingdomCards = 10;
    }
    return self;
}


// TODO: Prio_low - Change access to through enum instead of through string? Enums are not objects though, so cannot work directly as key in a dict (But maybe their int value as NSNumber?). On the other hand this is most likely temporary, until values can be chosen through GUI.
- (NSDictionary *)setupCardStackSizes
{
    return @{
      // Players in game:   0    1    2    3    4
       @"victory"      : @[@-1, @-1, @8,  @12, @12],
       @"gold"         : @[@-1, @-1, @16, @20, @24],
       @"silver"       : @[@-1, @-1, @24, @30, @36],
       @"copper"       : @[@-1, @-1, @24, @30, @36],
       @"curse"        : @[@-1, @-1, @10, @20, @30],
       @"kingdom"      : @[@-1, @-1, @10, @10, @10],
    };
}


#pragma mark Player methods


// Return if new Player is added to array.
- (BOOL)addHumanPlayerWithName:(NSString *)playerName
{
    if ([_playersOfTheGame count] >= _maximumPlayers
        || [self playerWithName:playerName])
    {
        return NO;
    }
    else
    {
        [_playersOfTheGame addObject:[[Player alloc]initWithName:playerName]];
        return YES;
    }
    
}


- (NSMutableArray *)playersAdded
{
    return _playersOfTheGame;
}


- (void)removePlayerWithName:(NSString *)playerName
{
    [_playersOfTheGame removeObject:[self playerWithName:playerName]];
}


- (Player *)playerWithName:(NSString *)playerName
{
    for (Player *addedPlayer in _playersOfTheGame) {
        if ([[addedPlayer playerName]isEqualToString:playerName])
        {
            return addedPlayer;
        }
    }
    return nil;
}


#pragma mark Card methods

- (void)addKingdomCardWithName:(NSString *)cardName
{
    if (![self cardWithName:cardName inArray:_kingdomCardsChosen])
    {
        Card *cardToAdd = [_cardVault produceCardFromName:cardName];
        if (cardToAdd && [cardToAdd conformsToProtocol:@protocol(KingdomCardProtocol)])
        {
            [_kingdomCardsChosen addObject:cardToAdd];
        }

    }
}


- (void)removeKingdomCardWithName:(NSString *)cardName
{
    Card *cardForRemoval = [self cardWithName:cardName inArray:_kingdomCardsChosen];
    if (cardForRemoval)
    {
        [_kingdomCardsChosen removeObject:cardForRemoval];
    }
}


- (void)addRandomKingdomCard
{
    //TODO: Prio_low - implement
    
}


- (Card *)cardWithName:(NSString *)cardName inArray:(NSArray *)array
{
    for (Card *card in _kingdomCardsChosen){
        if ([[card cardName]isEqualToString:cardName])
        {
            return card;
        }
    }
    return nil;
}


// TODO: Prio_medium - make abstract CardStack and implement new concrete supply stack?
- (CardStack *)stackOfCard:(Card *)card withCategory:(NSString *)category
{
    int currentAmountOfPlayers = [_playersOfTheGame count];
    int stackSize = [[[_cardStackSizes objectForKey:category]objectAtIndex:currentAmountOfPlayers]intValue];
    return [[CardStack alloc]initWithCard:card
                             andStackSize:stackSize];
}


- (NSArray *)constructCardSupply // TODO: Prio_low - Have a start game method instead? (Related to 
{
    if ([self isGameReady])
    {
    return [[NSArray arrayWithArray      :[self constructSupply_BaseCards]]
            arrayByAddingObjectsFromArray:[self constructSupply_KingdomCards]];
    }
    else
    {
        return nil;
    }
}


- (bool)isGameReady
{
    return  [_playersOfTheGame count] >= _minimumPlayers
    &&      [_playersOfTheGame count] <= _maximumPlayers
    &&      [_kingdomCardsChosen count] >= _minimumKingdomCards
    &&      [_kingdomCardsChosen count] <= _maximumKingdomCards ;
}


- (NSArray *)constructSupply_BaseCards
{
               // First the 3 victory cards
    return @[  [self stackOfCard:[_cardVault produceCardFromName:@"Estate"]   withCategory:@"victory"],
               [self stackOfCard:[_cardVault produceCardFromName:@"Duchy"]    withCategory:@"victory"],
               [self stackOfCard:[_cardVault produceCardFromName:@"Province"] withCategory:@"victory"],
               // Then the curse card
               [self stackOfCard:[_cardVault produceCardFromName:@"Curse"]    withCategory:@"curse"],
               // Then the 3 treasure cards
               [self stackOfCard:[_cardVault produceCardFromName:@"Copper"]   withCategory:@"copper"],
               [self stackOfCard:[_cardVault produceCardFromName:@"Silver"]   withCategory:@"silver"],
               [self stackOfCard:[_cardVault produceCardFromName:@"Gold"]     withCategory:@"gold"],    ];
}


- (NSArray *)constructSupply_KingdomCards
{
    NSMutableArray *supplyOfKingdomCards = [[NSMutableArray alloc]init];
    for (Card *card in _kingdomCardsChosen) {
        [supplyOfKingdomCards addObject:[self stackOfCard:card withCategory:@"kingdom"]];
    }
    return supplyOfKingdomCards;
}


// In the standard game, every player deck has 3 Estates and 7 Coppers.
- (NSArray *)constructPlayerStartingCards
{
    return @[ [_cardVault produceCardFromName:@"Estate"],
              [_cardVault produceCardFromName:@"Estate"],
              [_cardVault produceCardFromName:@"Estate"],
              
              [_cardVault produceCardFromName:@"Copper"],
              [_cardVault produceCardFromName:@"Copper"],
              [_cardVault produceCardFromName:@"Copper"],
              [_cardVault produceCardFromName:@"Copper"],
              [_cardVault produceCardFromName:@"Copper"],
              [_cardVault produceCardFromName:@"Copper"],
              [_cardVault produceCardFromName:@"Copper"]  ];
}



@end
