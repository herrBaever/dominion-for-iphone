//
//  Game.m
//  Dominion
//
//  Created by Jonas Illum on 30/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "Game.h"
#import "Card.h"
#import "CardType.h"
#import "CardStack.h"

@interface Game ()

@property (nonatomic) int playerCount;
@property (nonatomic, readwrite) NSMutableArray *playersInTurnOrder;
@property (nonatomic) Player *activePlayer;
@property (nonatomic, readwrite) NSArray *cardSupply;

@end

@implementation Game


#pragma mark Initilisation

- (instancetype)initWithPreparation:(GamePreparation *)gamePreparation
{
    self = [super init];
    if (self)
    {
        _roundNumber = @1;
        _playerCount = [[gamePreparation playersAdded]count];
        _playersInTurnOrder = [self randomTurnOrderWithPlayers:[gamePreparation playersAdded]];
        _cardSupply = [gamePreparation constructCardSupply];
        
        [self startGameWithPreparation:gamePreparation];
    }
    return self;
}


- (NSMutableArray *)randomTurnOrderWithPlayers:(NSMutableArray *)players
{
    NSMutableArray *newOrder = [[NSMutableArray alloc]initWithArray:players];
    int totalShufles = [players count];
    int shuflesLeftToDo = totalShufles;
    int progressedToIndex = 0;
    while (shuflesLeftToDo > 0)
    {
        [newOrder exchangeObjectAtIndex:progressedToIndex
                      withObjectAtIndex:arc4random_uniform(shuflesLeftToDo)]; // A player object can and must be able to end up on its original position.
        progressedToIndex++;
        shuflesLeftToDo--;
    }
    return newOrder;
}


- (void)startGameWithPreparation:(GamePreparation *)gamePreparation
{
    for (Player *player in _playersInTurnOrder)
    {
        for (Card *card in [gamePreparation constructPlayerStartingCards])
        {
            [player giveCard:card];
        }
    }
    [self nextTurn];
}


- (void)nextTurn
{
    if (_activePlayer) [_playersInTurnOrder addObject:_activePlayer]; // Player that just took turn is placed in the back of the queue.
    [self setActivePlayer:[_playersInTurnOrder firstObject]]; // First player in queue is now active player.
    [_playersInTurnOrder removeObjectAtIndex:0];
}

@end
