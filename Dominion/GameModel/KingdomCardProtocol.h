//
//  KingdomCardProtocol
//  Dominion
//
//  Created by Jonas Illum on 30/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol KingdomCardProtocol <NSObject>

- (void)playAction;

@end
