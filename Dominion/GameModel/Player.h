//
//  Player.h
//  Dominion
//
//  Created by Jonas Illum on 01/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"
#import "PlayerHand.h"
#import "PlayerPlayField.h"

@interface Player : NSObject


@property (nonatomic, readonly) NSString *playerName;
@property (nonatomic, readonly) PlayerHand *playerHand;
@property (nonatomic, readonly) PlayerPlayField *playField;

- (instancetype)initWithName:(NSString *)playerName;

- (void)giveCard:(Card *)card;
- (void)beginTurn;
   
@end
