//
//  PlayerPlayField.h
//  Dominion
//
//  Created by Jonas Illum on 09/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"


@interface PlayerPlayField : NSObject

- (void)playCard:(Card *)card;
- (NSArray *)cleanUp;

@end
