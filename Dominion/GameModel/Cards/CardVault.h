//
//  CardVault.h
//  Dominion
//
//  Created by Jonas Illum on 01/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO: Prio_medium - How to avoid giant list of card imports? Check Foundation.h for a hint?
#import "WitchCard.h"
#import "CellarCard.h"
#import "ChapelCard.h"
#import "LaboratoryCard.h"
#import "MarketCard.h"
#import "RemodelCard.h"
#import "SmithyCard.h"
#import "VillageCard.h"
#import "WoodcutterCard.h"
#import "WorkshopCard.h"
#import "CurseCard.h"
#import "EstateCard.h"
#import "DuchyCard.h"
#import "ProvinceCard.h"
#import "CopperCard.h"
#import "SilverCard.h"
#import "GoldCard.h"


@interface CardVault : NSObject

+ (CardVault *)sharedInstance;
- (Card *)produceCardFromName:(NSString *)cardName;
- (int)numberOfCards;
- (NSArray *)produceRandomUniqueKingdomCardsWithCount:(int)count;
- (NSArray *)allCards;

@end
