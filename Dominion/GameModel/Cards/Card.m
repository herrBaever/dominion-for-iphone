//
//  Card.m
//  Dominion
//
//  Created by Jonas Illum on 29/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//
//  - Abstract Card -

#import "Card.h"

@interface Card()

@property (nonatomic, readwrite) NSString *cardName;
@property (nonatomic, readwrite) NSString *cardText;
@property (nonatomic, readwrite) CardType *cardType;
@property (nonatomic, readwrite) NSNumber *cardPrice;

@end

@implementation Card


#pragma mark Initilisation

// Designated initialiser
- (instancetype)initCardWithName:(NSString *)name
                            text:(NSString *)text
                        cardType:(CardType *)cardType
                           price:(NSNumber *)price
{
    if (self = [super init])
    {
        _cardName = name;
        _cardText = text;
        _cardType = cardType;
        _cardPrice = price;
    }
    return self;
}


// A copy will retain full state. Values are static through the game.
- (id)copyWithZone:(NSZone *)zone
{
    Card *newCopyCard = [[[self class] allocWithZone:zone]init];
    [newCopyCard setCardName :[self cardName]];
    [newCopyCard setCardText :[self cardText]];
    [newCopyCard setCardType :[self cardType]];
    [newCopyCard setCardPrice:[self cardPrice]];
    return newCopyCard;
}

@end