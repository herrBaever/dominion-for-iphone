//
//  TreasureCardProtocol.h
//  Dominion
//
//  Created by Jonas Illum on 08/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TreasureCardProtocol <NSObject>

- (void)playCoin;

@end
