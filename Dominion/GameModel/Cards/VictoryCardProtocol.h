//
//  VictoryCardProtocol.h
//  Dominion
//
//  Created by Jonas Illum on 11/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol VictoryCardProtocol <NSObject>

- (int)victoryPoints;

@end
