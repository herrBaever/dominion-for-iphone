//
//  CardVault.m
//  Dominion
//
//  Created by Jonas Illum on 01/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardVault.h"

@interface CardVault ()

@property (nonatomic) NSDictionary *vault;

@end


static const int MAX_RANDOM_KINGDOMCARD_REQUEST = 10; // 10 largest kingdom supply a game is played with.


@implementation CardVault

#pragma mark Initilisation

+ (CardVault *)sharedInstance
{
    static CardVault *Vault;
        // One way to implement a singleton in obj-c:
        static dispatch_once_t dispatchOnlyOnce; // Holds the state of if next block has been dispatched
        dispatch_once(&dispatchOnlyOnce,
                      ^{ // Now dispatching the block. Remember that '&' indicates a reference in C.
                          Vault = [[self alloc]init];
                      });
        return Vault;
}


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self constructCardVaultDictionary];
    }
    return self;
}


- (void)constructCardVaultDictionary
{
    _vault = @{ @"Witch"        :[[WitchCard alloc]init],
                @"Cellar"       :[[CellarCard alloc]init],
                @"Chapel"       :[[ChapelCard alloc]init],
                @"Laboratory"   :[[LaboratoryCard alloc]init],
                @"Market"       :[[MarketCard alloc]init],
                @"Remodel"      :[[RemodelCard alloc]init],
                @"Smithy"       :[[SmithyCard alloc]init],
                @"Village"      :[[VillageCard alloc]init],
                @"Woodcutter"   :[[WoodcutterCard alloc]init],
                @"Curse"        :[[CurseCard alloc]init],
                @"Estate"       :[[EstateCard alloc]init],
                @"Duchy"        :[[DuchyCard alloc]init],
                @"Province"     :[[ProvinceCard alloc]init],
                @"Copper"       :[[CopperCard alloc]init],
                @"Silver"       :[[SilverCard alloc]init],
                @"Gold"         :[[GoldCard alloc]init],
                @"Workshop"     :[[WorkshopCard alloc]init],
            };
}


#pragma mark Card Production

- (Card *)produceCardFromName:(NSString *)cardName
{
    return [_vault valueForKey:cardName];
}


- (NSArray *)produceRandomUniqueKingdomCardsWithCount:(int)count
{
    // Returns nil if requested amount of cards is above the vaults size or below 1
    if (count > MAX_RANDOM_KINGDOMCARD_REQUEST  ||  count < 1)  return nil;
    
    NSMutableArray *producedCards = [[NSMutableArray alloc]init];
    NSMutableArray *allCardsInVault = [NSMutableArray arrayWithArray:[[self vault]allValues]];
    while (count > 0)
    {
        int randomIndex = arc4random_uniform([allCardsInVault count]);
        Card *randomCard = [allCardsInVault objectAtIndex:randomIndex];
        if ([randomCard conformsToProtocol:@protocol(KingdomCardProtocol)])
        {
            [producedCards addObject:randomCard];
            count--;
        }
        [allCardsInVault removeObject:randomCard]; // So that produced cards remains unique. Cards that didn't conform to KingdomCardProtocol will also not be picked again.
    }
    return producedCards;
}


- (NSArray *)allCards
{
    return [[self vault]allValues];
}


#pragma mark Misc methods

- (int)numberOfCards
{
    return [[self vault]count];
}


@end
