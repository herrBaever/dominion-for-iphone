//
//  CardType.m
//  Dominion
//
//  Created by Jonas Illum on 29/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CardType.h"


@interface CardType()

@property NSArray *subTypes;

@end


@implementation CardType



#pragma mark Initialisation

- (instancetype)initWithCardSubType:(CardSubType) subType
{
    self = [super init];
    if (self)
    {
        [self flagSubTypeWith:subType];
        _subTypeCount = 1;
    }
    return self;
}

- (instancetype)initWithCardSubType:(CardSubType) firstSubType andSubType:(CardSubType) secondSubType
{
    self = [self initWithCardSubType:firstSubType];
    if (self)
    {
        [self flagSubTypeWith:secondSubType];
        _subTypeCount = 2;
    }
    return self;
}

- (instancetype)initWithCardSubTypes:(CardSubType) firstSubType andSubType:(CardSubType) secondSubType andSubType: (CardSubType)thirdSubType
{
    self = [self initWithCardSubType:firstSubType andSubType:secondSubType];
    if (self)
    {
        [self flagSubTypeWith:thirdSubType];
        _subTypeCount = 3;
        
    }
    return self;
}


- (void)flagSubTypeWith:(CardSubType)subTypeEnumeration
{
    switch (subTypeEnumeration)
    {
        case VictorySubType:  _victory  = YES; break;
        case TreasureSubType: _treasure = YES; break;
        case ActionSubType:   _action   = YES; break;
        case CurseSubType:    _curse    = YES; break;
        case ReactionSubType: _reaction = YES; break;
        case AttackSubType:   _attack   = YES; break;
        case LooterSubType:   _looter   = YES; break;
        case PrizeSubType:    _prize    = YES; break;
        case ShelterSubType:  _shelter  = YES; break;
        case KnightSubType:   _knight   = YES; break;
        case DurationSubType: _duration = YES; break;
            
        default: [NSException raise:@"Invalid CardSubType" format:@"CardSubType of enum value %d is invalid", subTypeEnumeration]; break;
    }
}


#pragma mark Query methods

// TODO: Prio_medium - Oh lord...
- (bool)hasSubType:(CardSubType)subType
{
    switch (subType)
    {
        case VictorySubType:  return [self isVictory]; break;
        case TreasureSubType: return [self isTreasure]; break;
        case ActionSubType:   return [self isAction]; break;
        case CurseSubType:    return [self isCurse]; break;
        case ReactionSubType: return [self isReaction]; break;
        case AttackSubType:   return [self isAttack]; break;
        case LooterSubType:   return [self isLooter]; break;
        case PrizeSubType:    return [self isPrize]; break;
        case ShelterSubType:  return [self isShelter]; break;
        case KnightSubType:   return [self isKnight]; break;
        case DurationSubType: return [self isDuration]; break;
        default: return NO;
    }
}


@end
