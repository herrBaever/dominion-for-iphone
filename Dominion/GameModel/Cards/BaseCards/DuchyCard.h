//
//  DuchyCard.h
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "Card.h"
#import "VictoryCardProtocol.h"


@interface DuchyCard : Card <VictoryCardProtocol>

@end
