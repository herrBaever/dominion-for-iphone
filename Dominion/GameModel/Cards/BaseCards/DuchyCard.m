//
//  DuchyCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "DuchyCard.h"
#import "CardType.h"

@implementation DuchyCard


- (instancetype)init
{
    self = [super initCardWithName:@"Duchy"
                              text:@""
                          cardType:[[CardType alloc]initWithCardSubType:VictorySubType]
                             price:@5];
    return self;
}


-(int)victoryPoints
{
    return 3;
}


@end
