//
//  ProvinceCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "ProvinceCard.h"
#import "CardType.h"

@implementation ProvinceCard


- (instancetype)init
{
    self = [super initCardWithName:@"Province"
                              text:@""
                          cardType:[[CardType alloc]initWithCardSubType:VictorySubType]
                             price:@8];
    return self;
}


-(int)victoryPoints
{
    return 6;
}


@end
