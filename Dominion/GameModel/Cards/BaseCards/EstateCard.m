//
//  EstateCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "EstateCard.h"
#import "CardType.h"

@implementation EstateCard


- (instancetype)init
{
    self = [super initCardWithName:@"Estate"
                              text:@""
                          cardType:[[CardType alloc]initWithCardSubType:VictorySubType]
                             price:@2];
    return self;
}


-(int)victoryPoints
{
    return 1;
}


@end
