//
//  GoldCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "GoldCard.h"
#import "CardType.h"

@implementation GoldCard

- (instancetype)init
{
    self = [super initCardWithName:@"Gold"
                              text:@""
                          cardType:[[CardType alloc]initWithCardSubType:TreasureSubType]
                             price:@6];
    return self;
}


- (void)playCoin
{
    // TODO: Prio_high - Implement
    NSLog(@"Gold played");
}


@end
