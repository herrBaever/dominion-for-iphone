//
//  SilverCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "SilverCard.h"
#import "CardType.h"

@implementation SilverCard

- (instancetype)init
{
    self = [super initCardWithName:@"Silver"
                              text:@""
                          cardType:[[CardType alloc]initWithCardSubType:TreasureSubType]
                             price:@3];
    return self;
}


- (void)playCoin
{
    // TODO: Prio_high - Implement
    NSLog(@"Silver played");
}


@end
