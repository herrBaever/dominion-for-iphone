//
//  CopperCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CopperCard.h"
#import "CardType.h"

@implementation CopperCard

- (instancetype)init
{
    self = [super initCardWithName:@"Copper"
                              text:@""
                          cardType:[[CardType alloc]initWithCardSubType:TreasureSubType]
                             price:@0];
    return self;
}


- (void)playCoin
{
    // TODO: Prio_high - Implement
    NSLog(@"Copper played");
}


@end
