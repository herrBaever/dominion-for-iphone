//
//  CurseCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CurseCard.h"
#import "CardType.h"

@implementation CurseCard


- (instancetype)init
{
    self = [super initCardWithName:@"Curse"
                              text:@""
                          cardType:[[CardType alloc]initWithCardSubType:CurseSubType]
                             price:@0];
    return self;
}


-(int)victoryPoints
{
    return -1;
}


@end
