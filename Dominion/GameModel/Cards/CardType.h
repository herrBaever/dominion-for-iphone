//
//  CardType.h
//  Dominion
//
//  Created by Jonas Illum on 29/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.

// TODO: Prio_medium - This class is such a mess. Listen to its cries for refactoring.

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, CardSubType)
{
    VictorySubType, TreasureSubType, ActionSubType, CurseSubType, ReactionSubType, AttackSubType, LooterSubType, PrizeSubType, ShelterSubType, KnightSubType, DurationSubType,
};


@interface CardType : NSObject

@property (nonatomic, readonly, getter=isVictory)  Boolean victory;
@property (nonatomic, readonly, getter=isTreasure) Boolean treasure;
@property (nonatomic, readonly, getter=isAction)   Boolean action;
@property (nonatomic, readonly, getter=isCurse)    Boolean curse;
@property (nonatomic, readonly, getter=isReaction) Boolean reaction;
@property (nonatomic, readonly, getter=isAttack)   Boolean attack;
@property (nonatomic, readonly, getter=isLooter)   Boolean looter;
@property (nonatomic, readonly, getter=isPrize)    Boolean prize;
@property (nonatomic, readonly, getter=isShelter)  Boolean shelter;
@property (nonatomic, readonly, getter=isKnight)   Boolean knight;
@property (nonatomic, readonly, getter=isDuration) Boolean duration;
@property (nonatomic, readonly) int subTypeCount;

- (instancetype)initWithCardSubType:(CardSubType) subType;
- (instancetype)initWithCardSubType:(CardSubType) firstSubType andSubType:(CardSubType) secondSubType;
- (instancetype)initWithCardSubTypes:(CardSubType) firstSubType andSubType:(CardSubType) secondSubType andSubType: (CardSubType)thirdSubType;

- (bool)hasSubType:(CardSubType)subType;

@end
