//
//  Card.h
//  Dominion
//
//  Created by Jonas Illum on 29/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//
//  - Abstract Card -

#import <Foundation/Foundation.h>
#import "CardType.h"

//@protocol CardDelegate

//@end


@interface Card : NSObject <NSCopying>

@property (nonatomic, readonly) NSString *cardName;
@property (nonatomic, readonly) NSString *cardText;
@property (nonatomic, readonly) CardType *cardType;
@property (nonatomic, readonly) NSNumber *cardPrice;

// For card creation on the fly. TODO: Prio_medium - Is this needed anymore?
- (instancetype)initCardWithName:(NSString *)name
                            text:(NSString *)text
                        cardType:(CardType *)cardType
                           price:(NSNumber *)price;

@end
