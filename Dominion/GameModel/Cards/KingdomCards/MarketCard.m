//
//  MarketCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "MarketCard.h"

#import "CardType.h"

@implementation MarketCard

- (instancetype)init
{
    self = [super initCardWithName:@"Market"
                              text:@"+1 Card; +1 Action; +1 Buy; +$1."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@5];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}

@end
