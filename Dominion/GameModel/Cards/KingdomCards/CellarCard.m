//
//  CellarCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "CellarCard.h"
#import "CardType.h"

@implementation CellarCard

- (instancetype)init
{
    self = [super initCardWithName:@"Cellar"
                              text:@"Discard any number of cards.+1 Card per card discarded."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@2];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}


@end
