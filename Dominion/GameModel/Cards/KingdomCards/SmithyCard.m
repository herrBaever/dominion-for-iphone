//
//  SmithyCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "SmithyCard.h"
#import "CardType.h"

@implementation SmithyCard

- (instancetype)init
{
    self = [super initCardWithName:@"Smithy"
                              text:@"+3 Cards."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@4];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}

@end
