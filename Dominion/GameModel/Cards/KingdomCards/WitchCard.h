//
//  WitchCard.h
//  Dominion
//
//  Created by Jonas Illum on 30/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "Card.h"
#import "KingdomCardProtocol.h"

@interface WitchCard : Card <KingdomCardProtocol>

@end
