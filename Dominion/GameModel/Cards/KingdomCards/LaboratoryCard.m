//
//  LaboratoryCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "LaboratoryCard.h"

#import "CardType.h"

@implementation LaboratoryCard

- (instancetype)init
{
    self = [super initCardWithName:@"Laboratory"
                              text:@"+2 Cards; +1 Action."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@5];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}

@end
