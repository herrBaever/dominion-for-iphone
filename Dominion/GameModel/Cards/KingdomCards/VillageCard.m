//
//  VillageCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "VillageCard.h"
#import "CardType.h"

@implementation VillageCard

- (instancetype)init
{
    self = [super initCardWithName:@"Village"
                              text:@"+1 Card; +2 Actions."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@3];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}

@end
