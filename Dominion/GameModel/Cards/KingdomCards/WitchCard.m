//
//  WitchCard.m
//  Dominion
//
//  Created by Jonas Illum on 30/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "WitchCard.h"
#import "CardType.h"

@implementation WitchCard

- (instancetype)init
{
    self = [super initCardWithName:@"Witch"
                              text:@"Draw two cards, each opponent gains a curse."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType andSubType:AttackSubType]
                             price:@5];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}


@end
