//
//  WorkshopCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "WorkshopCard.h"
#import "CardType.h"

@implementation WorkshopCard

- (instancetype)init
{
    self = [super initCardWithName:@"Workshop"
                              text:@"Gain a card costing up to $4."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@3];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}

@end
