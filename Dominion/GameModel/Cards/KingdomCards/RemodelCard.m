//
//  RemodelCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "RemodelCard.h"
#import "CardType.h"

@implementation RemodelCard

- (instancetype)init
{
    self = [super initCardWithName:@"Remodel"
                              text:@"Trash a card from your hand. Gain a card costing up to $2 more than the trashed card."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@4];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}

@end
