//
//  WoodcutterCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "WoodcutterCard.h"
#import "CardType.h"

@implementation WoodcutterCard

- (instancetype)init
{
    self = [super initCardWithName:@"Woodcutter"
                              text:@"+1 Buy; +$2."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@3];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}

@end
