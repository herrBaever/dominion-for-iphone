//
//  ChapelCard.m
//  Dominion
//
//  Created by Jonas Illum on 02/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "ChapelCard.h"

#import "CardType.h"

@implementation ChapelCard

- (instancetype)init
{
    self = [super initCardWithName:@"Chapel"
                              text:@"Trash up to 4 cards from your hand."
                          cardType:[[CardType alloc]initWithCardSubType:ActionSubType]
                             price:@2];
    return self;
}

- (void)playAction
{
    //TODO: Prio_high - Implement action
    NSLog(@"%@ plays action!", [self cardText]);
}

@end
