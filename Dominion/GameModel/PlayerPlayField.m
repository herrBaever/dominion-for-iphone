//
//  PlayerPlayField.m
//  Dominion
//
//  Created by Jonas Illum on 09/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "PlayerPlayField.h"

@interface PlayerPlayField ()

@property NSMutableArray *playField;

@end


@implementation PlayerPlayField

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _playField = [[NSMutableArray alloc]init];
    }
    return self;
}


- (void)playCard:(Card *)card
{
    [_playField addObject:card];
}


- (NSArray *)cleanUp
{
    NSMutableArray *cleanedCards = [[NSMutableArray alloc]init];
    for (Card *card in _playField)
    {
        if (![[card cardType]isDuration])
        {
            [cleanedCards addObject:card];
        }
        else if ([[card cardType]isDuration])
        {
            // TODO: Prio_medium - implement duration mechanics.
        }
    }
    return cleanedCards;
}


@end
