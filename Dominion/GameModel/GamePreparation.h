//
//  GamePreparation.h
//  Dominion
//
//  Created by Jonas Illum on 01/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//
// Prepares a new game. Game object starts a game by using the data from this object.

// TODO: Prio_high - Follow the builder pattern closer.


#import <Foundation/Foundation.h>
#import "Player.h"


@interface GamePreparation : NSObject

//- (void)addPlayer:(Player *)player;
//- (void)removePlayer:(Player *)player;
//- (void)addRandomKingdomCard;

@property (nonatomic, readonly) int minimumPlayers;
@property (nonatomic, readonly) int maximumPlayers;

@property (nonatomic, readonly) int minimumKingdomCards;
@property (nonatomic, readonly) int maximumKingdomCards;

- (BOOL)addHumanPlayerWithName:(NSString *)playerName;
- (void)removePlayerWithName:(NSString *)playerName;
- (NSMutableArray *)playersAdded;

- (void)addKingdomCardWithName:(NSString *)cardName;
- (void)removeKingdomCardWithName:(NSString *)cardName;
- (NSArray *)kingdomCardsChosen;
- (NSArray *)constructCardSupply;
- (NSArray *)constructPlayerStartingCards;

- (bool)isGameReady;

@end
