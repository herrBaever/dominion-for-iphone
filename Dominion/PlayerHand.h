//
//  PlayerHand.h
//  Dominion
//
//  Created by Jonas Illum on 08/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardType.h"
#import "Card.h"


@interface PlayerHand : NSObject

- (int)handSize;
- (BOOL)hasCardOfSubType:(CardSubType)subType;

- (void)addCard:(Card *)card;
- (NSArray *)cards;
- (NSArray *)transferAllCards;

@end
