//
//  NotificationList.h
//  Dominion
//
//  Created by Jonas Illum on 06/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

// To have one single place for the notification strings.
// Import where we need to use NSNotificationcenter

// extern keyword in c: http://en.wikipedia.org/wiki/External_variable

#import <Foundation/Foundation.h>

// Model to view
extern NSString * const NewTurnDidStartNotification;
extern NSString * const GameDidFinishNotification;

extern NSString * const AKingdomSupplyDidEmptyNotification;
extern NSString * const ProviceSupplyDidEmptyNotification;

extern NSString * const PlayerDidBeginBuyPhaseNotification;
extern NSString * const PlayerDidEndBuyPhaseNotification;

extern NSString * const PlayerDidBeginActionPhaseNotification;
extern NSString * const PlayerDidEndActionPhaseNotification;

extern NSString * const PlayerDidBeginCleanUpPhaseNotification;
extern NSString * const PlayerDidEndCleanUpPhaseNotification;


// Game Preparation
extern NSString * const Prep_PlayerWasAddedNotification;
extern NSString * const Prep_PlayerWasRemovedNotification;
extern NSString * const Prep_GameDidStartNotification;

// and so on...
