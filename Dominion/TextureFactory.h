//
//  TextureFactory.h
//  Dominion
//
//  Created by Jonas Illum on 14/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface TextureFactory : NSObject

+ (SKTexture *)cardTextureWithFileName:(NSString *)fileName;
+ (SKTexture *)miscTextureWithFileName:(NSString *)fileName;

@end
