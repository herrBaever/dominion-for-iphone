//
//  NotificationList.m
//  Dominion
//
//  Created by Jonas Illum on 06/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "NotificationList.h"

// It doesn't matter what the strings says, as long as they remain unique to eachother.
// Make sure this implementation file has the correct targets set.

// Model to view
NSString * const NewTurnDidStartNotification = @"newTurn";
NSString * const GameDidFinishNotification = @"gameFinished";


// View to model
NSString * const PlayerDidBeginBuyPhaseNotification = @"playerBeginBuyPhase";
NSString * const PlayerDidEndBuyPhaseNotification = @"playerEndBuyPhase";

NSString * const PlayerDidBeginActionPhaseNotification = @"playerBeginActionPhase";
NSString * const PlayerDidEndActionPhaseNotification = @"playerEndActionPhase";

NSString * const PlayerDidBeginCleanUpPhaseNotification = @"playerBeginCleanUpPhase";
NSString * const PlayerDidEndCleanUpPhaseNotification = @"playerEndCleanUpPhase";


NSString * const Prep_PlayerWasAddedNotification = @"playerWasAdded";
NSString * const Prep_PlayerWasRemovedNotification = @"playerWasRemoved";
NSString * const Prep_GameDidStartNotification = @"gameDidStart";