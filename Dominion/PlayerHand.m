//
//  PlayerHand.m
//  Dominion
//
//  Created by Jonas Illum on 08/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "PlayerHand.h"


@interface PlayerHand ()

@property NSMutableArray *cardsInHand;

@end



@implementation PlayerHand


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _cardsInHand = [[NSMutableArray alloc]init];
    }
    return self;
}


- (int)handSize
{
    return [_cardsInHand count];
}


- (BOOL)hasCardOfSubType:(CardSubType)subType
{
    for (Card *card in _cardsInHand)
    {
        if ([[card cardType]hasSubType:subType])
        {
            return YES;
        }
    }
    return NO;
}


- (void)addCard:(Card *)card
{
    [_cardsInHand addObject:card];
}


- (NSArray *)transferAllCards
{
    NSArray *removedCards = [NSArray arrayWithArray:_cardsInHand];
    [_cardsInHand removeAllObjects];
    return removedCards;
}


- (NSArray *)cards
{
    return [NSArray arrayWithArray:_cardsInHand];
}


@end
