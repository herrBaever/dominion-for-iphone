//
//  GamePreparationScene.m
//  Dominion
//
//  Created by Jonas Illum on 13/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "GamePreparationScene.h"
#import "GameButton.h"
#import "NotificationList.h"
#import "TextureFactory.h"

static const float ButtonScaleFactor = 0.5;

@interface GamePreparationScene ()

@property (nonatomic) GameButton *testButton;
@property (nonatomic) GameButton *startGameButton;
@property (nonatomic) GameButton *addPlayerButton;

@end

@implementation GamePreparationScene


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self)
    {
        [self setBackgroundColor:[UIColor colorWithRed:(28.0/255) green:(134.0/255) blue:(47.0/255) alpha:1.0]];
        SKSpriteNode *playerArea = [[SKSpriteNode alloc]initWithTexture:[TextureFactory miscTextureWithFileName:@"panel_beigeLight.png"]];
        [playerArea setZPosition:0.01];
        
//        playerArea.frame = CGRectInset(self.frame, -20, -10);
        [playerArea setSize:CGSizeMake(self.size.width / 3, self.size.height - 70)];
        [playerArea setAnchorPoint:CGPointMake(1.0, 1.0)];
        [playerArea setPosition:CGPointMake(self.size.width - 10, self.size.height - 10)];
        [self addChild:playerArea];
//        [playerArea setAnchorPoint:CGPointMake(0.5, 0.5)];
        
        _startGameButton = [[GameButton alloc]initWithImageNamed:@"buttonLong_grey.png" notificationEvent:Prep_GameDidStartNotification  labelName:@"Start Game"];
        [self addButton:_startGameButton atPosX:playerArea.position.x - playerArea.size.width / 2
                                           posY:(self.size.height - playerArea.size.height - 10) / 2 ];

        
        _testButton = [[GameButton alloc]initWithImageNamed:@"buttonLong_grey.png" notificationEvent:nil labelName:@"Test"];
        [_testButton setScale:0.5];
        [_testButton setPosition:CGPointMake(self.size.width / 2, self.size.height / 2 + 30)];
        [self addChild:_testButton];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(disableTestButton) name:Prep_GameDidStartNotification object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enableTestButton) name:GameDidFinishNotification object:nil];
        
        GameButton *otherButton = [[GameButton alloc]initWithImageNamed:@"buttonLong_grey.png" notificationEvent:GameDidFinishNotification labelName:@"Enable"];
        [otherButton setScale:0.5];
        [otherButton setPosition:CGPointMake(self.size.width / 2, self.size.height / 2 + 60)];
        [self addChild:otherButton];
    }
    return self;
}


- (void)addButton:(GameButton *)button atPosX:(float)x posY:(float)y
{
    [button setPosition:CGPointMake(x, y)];
    [button setScale:ButtonScaleFactor];
    [self addChild:button];
}


- (void)disableTestButton
{
    [_testButton disable];
}


- (void)enableTestButton
{
    [_testButton enable];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    NSLog(@"\nX Coord: %f \nY Coord: %f", touchLocation.x, touchLocation.y);
}

@end
