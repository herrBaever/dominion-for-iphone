//
//  TextureFactory.m
//  Dominion
//
//  Created by Jonas Illum on 14/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "TextureFactory.h"

static SKTextureAtlas *cardTextureAtlas;
static SKTextureAtlas *miscTextureAtlas;

@implementation TextureFactory


+ (void)initialize
{
    miscTextureAtlas = [SKTextureAtlas atlasNamed:@"misc.atlas"];
    cardTextureAtlas = [SKTextureAtlas atlasNamed:@"cards.atlas"];
}


+ (SKTexture *)cardTextureWithFileName:(NSString *)fileName
{
    return [cardTextureAtlas textureNamed:fileName];
}


+ (SKTexture *)miscTextureWithFileName:(NSString *)fileName
{
    return [miscTextureAtlas textureNamed:fileName];
}


@end
