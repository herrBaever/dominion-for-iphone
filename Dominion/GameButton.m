//
//  GameButton.m
//  Dominion
//
//  Created by Jonas Illum on 13/10/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "GameButton.h"
#import "TextureFactory.h"

static NSString *pressedPostFix = @"_pressed";

@interface GameButton()

@property (nonatomic) NSString *notificationEvent;
@property (nonatomic) SKTexture *unpressedTexture;
@property (nonatomic) SKTexture *pressedTexture;
@property (nonatomic) SKLabelNode *buttonLabel;
@property (nonatomic) BOOL enabled;

@end



@implementation GameButton

#pragma mark Initilisation

- (instancetype)initWithImageNamed:(NSString *)name
              notificationEvent:(NSString *)notification
                      labelName:(NSString *)labelName
{
    _unpressedTexture = [TextureFactory miscTextureWithFileName:name];
    self = [super initWithTexture:_unpressedTexture];
    _pressedTexture = [TextureFactory miscTextureWithFileName:[self appendPostfix:pressedPostFix toFileName:name]];
    _notificationEvent = notification;
    [self enable];
    if (labelName)
    {
        _buttonLabel = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
        [_buttonLabel setFontColor:[UIColor blackColor]];
        [_buttonLabel setPosition:CGPointMake(0, -self.size.height / 4)];
        [_buttonLabel setText:labelName];
        [self addChild:_buttonLabel];
    }
    return self;
}


- (instancetype)initWithImageNamed:(NSString *)name
                 notificationEvent:(NSString *)notification
{
    return [self initWithImageNamed:name notificationEvent:notification labelName:nil];
}


- (NSString *)appendPostfix:(NSString *)postFix toFileName:(NSString *)fileName
{
    return [[[[fileName  stringByDeletingPathExtension]
             stringByAppendingString:postFix]
            stringByAppendingString:@"."]
            stringByAppendingString:[fileName pathExtension]  ];
}


#pragma mark Enabling / Disable Button

- (void)enable
{
    [self setEnabled:YES];
    [self setUserInteractionEnabled:YES];
    [self setAlpha:1.0];
}


- (void)disable
{
    [self setEnabled:NO];
    [self setUserInteractionEnabled:NO];
    [self setAlpha:0.5];
}


#pragma mark Touch methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // TODO: Prio_low - add particle effect?
    // TODO: Prio_low - add click sound
    [self setTexture:_pressedTexture];
    [_buttonLabel setPosition:CGPointMake(_buttonLabel.position.x, _buttonLabel.position.y - 1.5)]; // To let label follow button press
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[NSNotificationCenter defaultCenter] postNotificationName:_notificationEvent object:nil];
    [self setTexture:_unpressedTexture];
    [_buttonLabel setPosition:CGPointMake(_buttonLabel.position.x, _buttonLabel.position.y + 1.5)];
}


@end
