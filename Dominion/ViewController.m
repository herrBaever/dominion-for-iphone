//
//  ViewController.m
//  Dominion
//
//  Created by Jonas Illum on 29/09/14.
//  Copyright (c) 2014 baeverSoft. All rights reserved.
//

#import "ViewController.h"
#import "GameBoardScene.h"
#import "GamePreparationScene.h"

@implementation ViewController


- (void)viewDidLoad
{

}


- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    SKScene *startScene = [GamePreparationScene sceneWithSize:skView.bounds.size];
    startScene.scaleMode = SKSceneScaleModeAspectFill;
    [skView presentScene:startScene];
}


- (BOOL)shouldAutorotate
{
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else
    {
        return UIInterfaceOrientationMaskAll;
    }
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
